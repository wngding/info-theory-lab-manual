---
title: 课程视频清单
---

注意：

- 课程视频放在 B 站，**登录 B 站**后，课程视频可以**高清**观看
- 视频中的代码文字较小，**最好在电脑上观看**视频，不适合在手机上观看

## 前言

- [info-0-01](https://www.bilibili.com/video/BV1Tr4y1r77i?p=1)
- [info-0-02](https://www.bilibili.com/video/BV1Tr4y1r77i?p=2)
- [info-0-03](https://www.bilibili.com/video/BV1Tr4y1r77i?p=3)

## 绪论

- [info-1-01](https://www.bilibili.com/video/BV1Tr4y1r77i?p=4)
- [info-1-02](https://www.bilibili.com/video/BV1Tr4y1r77i?p=5)
- [info-1-03](https://www.bilibili.com/video/BV1Tr4y1r77i?p=6)
- [info-1-04](https://www.bilibili.com/video/BV1Tr4y1r77i?p=7)

## 信息的统计度量

### 自信息量和条件自信息量

- [info-2-01](https://www.bilibili.com/video/BV1Tr4y1r77i?p=8)
- [info-2-02](https://www.bilibili.com/video/BV1Tr4y1r77i?p=9)
- [info-2-03](https://www.bilibili.com/video/BV1Tr4y1r77i?p=10)
- [info-2-04](https://www.bilibili.com/video/BV1Tr4y1r77i?p=11)

### 互信息量和条件互信息量

- [info-2-05](https://www.bilibili.com/video/BV1Tr4y1r77i?p=12)
- [info-2-06](https://www.bilibili.com/video/BV1Tr4y1r77i?p=13)
- [info-2-07](https://www.bilibili.com/video/BV1Tr4y1r77i?p=14)
- [info-2-08](https://www.bilibili.com/video/BV1Tr4y1r77i?p=15)

### 离散集的平均自信息量

- [info-2-09](https://www.bilibili.com/video/BV1Tr4y1r77i?p=16)
- [info-2-10](https://www.bilibili.com/video/BV1Tr4y1r77i?p=17)
- [info-2-11](https://www.bilibili.com/video/BV1Tr4y1r77i?p=18)
- [info-2-12](https://www.bilibili.com/video/BV1Tr4y1r77i?p=19)
- [info-2-13](https://www.bilibili.com/video/BV1Tr4y1r77i?p=20)
- [info-2-14](https://www.bilibili.com/video/BV1Tr4y1r77i?p=21)
- [info-2-15](https://www.bilibili.com/video/BV1Tr4y1r77i?p=22)
- [info-2-16](https://www.bilibili.com/video/BV1Tr4y1r77i?p=23)

### 离散集的平均互信息量

- [info-2-17](https://www.bilibili.com/video/BV1Tr4y1r77i?p=24)
- [info-2-18](https://www.bilibili.com/video/BV1Tr4y1r77i?p=25)
- [info-2-19](https://www.bilibili.com/video/BV1Tr4y1r77i?p=26)
- [info-2-20](https://www.bilibili.com/video/BV1Tr4y1r77i?p=27)
- [info-2-21](https://www.bilibili.com/video/BV1Tr4y1r77i?p=28)
- [info-2-22](https://www.bilibili.com/video/BV1Tr4y1r77i?p=29)

## 离散信源

### 信源的数学模型及其分类
### 离散无记忆信源
### 离散无记忆信源的扩展信源
### 离散平稳信源

- [info-3-01](https://www.bilibili.com/video/BV1Tr4y1r77i?p=30)
- [info-3-02](https://www.bilibili.com/video/BV1Tr4y1r77i?p=31)
- [info-3-03](https://www.bilibili.com/video/BV1Tr4y1r77i?p=32)
- [info-3-04](https://www.bilibili.com/video/BV1Tr4y1r77i?p=33)
- [info-3-05](https://www.bilibili.com/video/BV1Tr4y1r77i?p=34)
- [info-3-06](https://www.bilibili.com/video/BV1Tr4y1r77i?p=35)
- [info-3-07](https://www.bilibili.com/video/BV1Tr4y1r77i?p=36)

### 马尔可夫信源
### 信源相关性和剩余度

- [info-3-08](https://www.bilibili.com/video/BV1Tr4y1r77i?p=37)
- [info-3-08-markov](https://www.bilibili.com/video/BV1Tr4y1r77i?p=38)
- [info-3-09](https://www.bilibili.com/video/BV1Tr4y1r77i?p=39)
- [info-3-10](https://www.bilibili.com/video/BV1Tr4y1r77i?p=40)
- [info-3-11](https://www.bilibili.com/video/BV1Tr4y1r77i?p=41)
- [info-3-12](https://www.bilibili.com/video/BV1Tr4y1r77i?p=42)
- [info-3-13](https://www.bilibili.com/video/BV1Tr4y1r77i?p=43)

## 离散信道及其容量

### 信道的数学模型及其分类
### 信道容量

- [info-4-01](https://www.bilibili.com/video/BV1Tr4y1r77i?p=44)
- [info-4-02](https://www.bilibili.com/video/BV1Tr4y1r77i?p=45)
- [info-4-03](https://www.bilibili.com/video/BV1Tr4y1r77i?p=46)
- [info-4-04](https://www.bilibili.com/video/BV1Tr4y1r77i?p=47)
- [info-4-05](https://www.bilibili.com/video/BV1Tr4y1r77i?p=48)
- [info-4-06](https://www.bilibili.com/video/BV1Tr4y1r77i?p=49)

### 离散无记忆信道

- [info-4-07](https://www.bilibili.com/video/BV1Tr4y1r77i?p=50)
- [info-4-08](https://www.bilibili.com/video/BV1Tr4y1r77i?p=51)
- [info-4-09](https://www.bilibili.com/video/BV1Tr4y1r77i?p=52)
- [info-4-10](https://www.bilibili.com/video/BV1Tr4y1r77i?p=53)
- [info-4-11](https://www.bilibili.com/video/BV1Tr4y1r77i?p=54)
- [info-4-12](https://www.bilibili.com/video/BV1Tr4y1r77i?p=55)

### 离散无记忆扩展信道
### 信道的组合

- [info-4-13](https://www.bilibili.com/video/BV1Tr4y1r77i?p=56)
- [info-4-14](https://www.bilibili.com/video/BV1Tr4y1r77i?p=57)
- [info-4-15](https://www.bilibili.com/video/BV1Tr4y1r77i?p=58)
- [info-4-16](https://www.bilibili.com/video/BV1Tr4y1r77i?p=59)
- [info-4-17](https://www.bilibili.com/video/BV1Tr4y1r77i?p=60)
- [info-4-18](https://www.bilibili.com/video/BV1Tr4y1r77i?p=61)

## 无失真信源编码

### 编码器
### 分组码

- [info-5-01](https://www.bilibili.com/video/BV1Tr4y1r77i?p=62)
- [info-5-02](https://www.bilibili.com/video/BV1Tr4y1r77i?p=63)
- [info-5-03](https://www.bilibili.com/video/BV1Tr4y1r77i?p=64)

### 定长码

- [info-5-04](https://www.bilibili.com/video/BV1Tr4y1r77i?p=65)
- [info-5-05](https://www.bilibili.com/video/BV1Tr4y1r77i?p=66)
- [info-5-06](https://www.bilibili.com/video/BV1Tr4y1r77i?p=67)

### 变长码

- 无
