---
title: 简介
description: 信息科学基础实验手册
next:
  link: /basic/lab01/
  label: 实验一、自信息量函数曲线
---

目录:

- [实验一、自信息量函数](basic/lab01/)  
- [实验二、熵函数](basic/lab02/)
- [实验三、数学之美](basic/lab03/)
- [实验四、BSC 信道仿真](basic/lab04/)
- [实验五、串联信道容量](basic/lab05/)
- [实验六、哈夫曼编码](basic/lab06/)
- [实验七、汉明编码](basic/lab07/)
