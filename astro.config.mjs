import { defineConfig } from 'astro/config';
import starlight from '@astrojs/starlight';
import remarkMath from 'remark-math'
import rehypeMathJax from 'rehype-mathjax'

export default defineConfig({
  server: { port: 8080, host: '192.168.14.128' },
  markdown: {
    remarkPlugins: [remarkMath],
    rehypePlugins: [rehypeMathJax],
  },
  integrations: [
    starlight({
      title: '信息科学基础实验手册',
      customCss: [
        './src/styles/custom.css',
      ],
      locales: {
        root: {
          label: '简体中文',
          lang: 'zh-CN',
        },
      },
      social: {
        github: 'https://bitbucket.org/wngding/manual',
      },
      sidebar: [
         { label: '信息科学基础在线实验', link: 'http://info-lab.wangding.co/' },
        {
          label: '目录',
          autogenerate: { directory: 'basic' },
        },
        {
          label: '附录',
          autogenerate: { directory: 'appendix' },
        },
      ],
    }),
  ],
});
